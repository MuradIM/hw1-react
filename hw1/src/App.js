import './App.css';
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import AcceptButton from "./components/Button/AcceptButton"
import DeclineButton from "./components/Button/DeclainButton"

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalInfo: {
                modalIsActive: false,
            },

        }
    }

    createModalHandler = buttonType => {
        this.setState({
            modalInfo: {
                modalIsActive: true,
                trigger: buttonType,
            },
        })
    }

    createModalProps = (type) => {
        if (type === "addButton") {
            return {
                header: 'Do you want to add this file?',
                closeButton: true,
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                actions: [AcceptButton],
                bg: 'green',
                color: 'white',
                closeHandler: this.closeModal.bind(this),
            }
        } else if (type === "removeButton") {
            return {
                header: 'Do you want to delete this file?',
                closeButton: false,
                text: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?',
                actions: [AcceptButton, DeclineButton],
                bg: 'red',
                color: 'white',
                closeHandler: this.closeModal.bind(this),
            }
        }
    }

    closeModal() {
        this.setState({
            modalInfo: {
                modalIsActive: false,
            },
        })
    }

    render() {
        return (
            <div className="App">
                <div>
                    <Button bg={"green"} text={"Open first modal"} createModal={this.createModalHandler}
                            buttonType={"addButton"}/>
                    <Button bg={"red"} text={"Open second modal"} createModal={this.createModalHandler}
                            buttonType={"removeButton"}/>
                </div>
                {this.state.modalInfo.modalIsActive ?
                    <Modal data={this.createModalProps(this.state.modalInfo.trigger)}/> : null}
            </div>
        )
    }
}

export default App;
