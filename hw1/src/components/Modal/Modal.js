import React from "react";
import styled from "styled-components";
import TitleWrapper from "../TitleWrapper"
import Wrapper from "../Wrapper";
import CloseButton from "../Button/CloseButton";

const StyledDiv = styled.div`
  cursor: pointer;
  margin: 0 auto;
  background-color: ${props => props.bg};
  color: ${props => props.color};
  width: 600px;
  height: auto;
  border-radius: 10px;
`

class Modal extends React.Component {
    render() {
        const values = this.props.data

        let actonButtons

        if (values.actions) {
            actonButtons = <div>
                {values.actions.map(e => e)}
            </div>
        }

        return (
            <Wrapper onClick={() => values.closeHandler()}>
                <StyledDiv {...values} onClick={e => e.stopPropagation()}>
                    <TitleWrapper>
                        <h4 style={{marginLeft: '10px'}}>
                            {values.header}
                        </h4>
                        {values.closeButton ? <CloseButton onClick={() => values.closeHandler()}/> : null}
                    </TitleWrapper>
                    <p style={{
                        margin: '10px',
                        lineHeight: '26px',
                    }}>
                        {values.text}
                    </p>
                    {values.actions ? actonButtons : null}
                </StyledDiv>
            </Wrapper>
        )
    }
}

export default Modal