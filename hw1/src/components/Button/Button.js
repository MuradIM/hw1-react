import React from "react";
import styled from "styled-components"

const StyledButton = styled.button`
  background-color: ${props => props.bg};
  width: 200px;
  height: 40px;
  font-size: 20px;
  border-radius: 5px;
`

class Button extends React.Component {

    clickHandler = () => {
        this.props.createModal(this.props.buttonType)
    }

    render() {

        return (
            <StyledButton {...this.props} onClick={this.clickHandler}>
                {this.props.text}
            </StyledButton>
        )
    }
}

export default Button