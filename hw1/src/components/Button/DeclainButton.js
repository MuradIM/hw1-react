import styled from "styled-components";

const StyledButton = styled.button`
  border: none;
  background-color: rgba(0, 0, 0, 0.3);
  border-radius: 10px;
  width: 120px;
  height: 30px;
  margin: 10px;
  color: white;

  &:hover {
    background-color: black;
  }
`

const Button = <StyledButton key={2}>
        Cancel
    </StyledButton>

export default Button