import styled from "styled-components";

const StyledDiv = styled.div`
  margin-top: 0;
  margin-bottom: 0;
  position: relative;
  background-color: rgba(0, 0, 0, 0.3);
  border-radius: 10px 10px 0 0;
  display: flex;
  justify-content: space-between;
`

const TitleWrapper = ({children}) => <StyledDiv>{children}</StyledDiv>

export default TitleWrapper