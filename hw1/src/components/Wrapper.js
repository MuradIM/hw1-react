import styled from "styled-components";

const StyledDiv = styled.div`
  position: fixed;
  height: 100vh;
  width: 100vw;
  background-color: rgba(0, 0, 0, 0.7);
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Wrapper = (props) => {
    const {children, onClick} = props

    return (
        <StyledDiv onClick ={onClick}>
            {children}
        </StyledDiv>
    )
}

export default Wrapper